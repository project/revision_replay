jQuery(document).ready(function() {
  jQuery('#slider').slider({
    min: 0,
    max: jQuery('.revision-replay-revision').length - 1,
    slide: function( event, ui ) {
      jQuery('.revision-replay-revision.active').removeClass('active');
      jQuery('#revision-replay-revision-' + ui.value).addClass('active');
    },
  });
});